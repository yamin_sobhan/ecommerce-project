<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <link href="{{asset('frontend/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/main.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/responsive.css')}}" rel="stylesheet">
          
    <link rel="shortcut icon" href="{{URL::to('frontend/images/ico/favicon.ico')}}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{URL::to('frontend/images/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{URL::to('frontend/images/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{URL::to('frontend/images/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{URL::to('frontend/images/ico/apple-touch-icon-57-precomposed.png')}}">
</head><!--/head-->

<style> 

.paymentWrap {
	padding: 50px;
}

.paymentWrap .paymentBtnGroup {
	max-width: 800px;
	margin: auto;
}

.paymentWrap .paymentBtnGroup .paymentMethod {
	padding: 40px;
	box-shadow: none;
	position: relative;
}

.paymentWrap .paymentBtnGroup .paymentMethod.active {
	outline: none !important;
}

.paymentWrap .paymentBtnGroup .paymentMethod.active .method {
	border-color: #4cd264;
	outline: none !important;
	box-shadow: 0px 3px 22px 0px #7b7b7b;
}

.paymentWrap .paymentBtnGroup .paymentMethod .method {
	position: absolute;
	right: 3px;
	top: 3px;
	bottom: 3px;
	left: 3px;
	background-size: contain;
	background-position: center;
	background-repeat: no-repeat;
	border: 2px solid transparent;
	transition: all 0.5s;
}

.paymentWrap .paymentBtnGroup .paymentMethod .method.visa {
	background-image: url("https://cdn1.vectorstock.com/i/1000x1000/08/85/money-hand-cash-vector-10260885.jpg");
}

.paymentWrap .paymentBtnGroup .paymentMethod .method.master-card {
	background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAWkAAACMCAMAAACJW6j5AAAA81BMVEX///8Am+EALIoAH2sALYsAl+AAluAAmeAAJogAKInByd0AKokAnOIAG4YAlN9KXaEyouMAIocAGIQAHoYAFGQrSZkAarGq2fSqstMAHGnh8/sAD4Lt8PcAFoQAAIDM0OTM6Pgrp+Xy+f0BK4Xd4/BzhLbf8PqGk74ADILm6vOz3PSd0/JTtOgTNo87r+cDR4+bqMsBitDR7fo2TpmIyu91wewDJ3rB4/dld69RZ6a2wNokQZSNm8Ndbqmr1PIoQpIADWICYKYDeL4CQYYCMXs9Vp59irkDVZsHc7gCV6cBR5wCbLoCPZUDc8ECXqxrueoACGukjjPcAAAUvElEQVR4nO1dC0PiSBIeYkMSGtAQ3QEhghHfwjg+Rnytu3M6s3fnnff/f80lna7q6pAHCC7sXb5xVAKB5Eul6quq7vbTpwLLh9+rzoTe4Nhf9jH/FeE/PDn1WeA6e8Ony5fH/VZvUDA+Pbq3TZsbSWDwRTaJRzyAbTlO02OXjw+D7rJP4S+CcV1RyIKfzEB2mQEP8GnyZLCNcdtxva3xXWHa+ei5HOxX8EzYZMSemRE9Q7bhNbEdb/jjeNknsvI4dIFTBqYrHjLpOJj8h4+YRjMTF8fg7tHjYNmnsuJYtxTHLO6ZGW4zDMqxTnj00+mMC4edhSdbOl2gjsWZR6dtoPtg+mb5Qse6W/bZrDC6e5zSqHEL3hk9BfEr0mvrz9rNH8s+n9VFz4lCnXIaQKWBTxCOUYcwvBq4a/jNKzxIGu7qyk+gYVMSSdwjTxia8yC3QPOxEHzJeKiTIKjcAtFxsQQGHUrsesA2r3AgyRg7Wlijkc9gTMVE0HxaXkPoV8R3zpd9TquJV5uBPUK0k4/RyIHjeH6oXDcRIszgTm/ZJ7WSEBniRO7HCM26TwE5iD8wrUHOrftln9QqYuBJ8mRcpJoNySaij4ZJZJ6ki2K7+7Ds01pB3DU1i6aiDjPyiEtivQZhFbMYYvLcLfTHBFp16mKVvKCGqxJukB7UxhlmlcqnF0Y9iR8O1RmKOQJaYmKGYei2TR0HXgvOln1eq4cXm5ir8gPKH5CiE00jVZ1J+R51MYyjQn7EYes2SuKcin1UU9O6qvolJrYZc4v0JYbuDrjgWI1DeWHlRUgU1FNGWoeKAijfKmKijmqHCAzqJjRJof7THBw2YFmPOpmjotCko9VkxEQ1Pw1OWDNf9QLquEkuA16nU132qa0YHlw2aaqYw2gmrtw4g6e1pqOhvUeztexTWzE8OgabMFEt0lGbJXzHJDetjogf7uGyT23FsGWT3A/LTPCQuJUJnQfmTPQhiaNGwbSOrsv1VhUlVPHPtA2EV13rgfIomJ7E4Ag9LLFRpF3xp9y3gUIFX6LKfMh2wXQMLQ8FMaqHGGckD6Q2r6sVtHisWRdM65CjaiiPYN6kOQtbDEYsHV2IzFUY0+6KgmkdorWF6Qf6W7TUvW9rU6Adfvv27dve3h7eFEU1T4O/bmvuV0NA9t40POsI+BZW3izG2FB0hxz9M7gJoureQXTEdmDaRY6o4dhDicbID/DMU7mOFLKLuoeGakeV4UghD8Lg+4leW/vPybJPbqUQVj1UIGREeIjNcxDdfm6MDpZ9eiuEfUcf6IFKTzC9Nw/TX2pm7XTZ57c6uLWIAqYBUXyfh+nN32sls3y97BNcFXS3bEzvIBnBkvNcAXHtt++1UslsFFRHGDAOHpoRsuG3uZguB0yXSo3CgQgMjgyNYFpKMuay6fazIDqgur/sk1wJtJqqDIdZOH7fm8OkQzctYO4WndtPkfSA1FupPJB6czH9XTJdqlwt+yxXAVJ6GER5EFk9F9MmMF3a2E75dH/M4hhuXd4+VD8yu/TfahPYHZ1d3aQdZRZO8B1y9vYvUXoou1YP5nHTX5DodKM+7PA4bNu2XHf4+HE1k6uKGaJk6iibtbP+zG82KsvdKzmBfyDqSypnoc2T4OccTG/+oZhO9dTrFtYBsKAYbgjnR3/YJNJds5SCsnk2o137+GaNm+xX9hzSXyGtKsn1+5lur5UJ02Y/+TiFxMRbioziCXtjrx9EtZnKdMD1xsVM77WN79XI2bHqqUahFNXUj8xh0j9r9ASS3cfxETILbUvV12HW8EOmoW830okOjKI2k1X38c3KOa8MW1u0J0vOl72/OL1GlUfkPhI/PRqnRjqS+uge93aWk54W/UoW06Xy2SxvdlrJPkOFWwtzFbAnfDgP0+2/UaJL5kaioz6vw+gGkjqhEGLGzkeMC/41m+lSpT/Dm12D98i9QFs2OA3wIGT87hxumsbDELVEL3boKE+l/ZTfnI8YF3yd4aaFVcxSqDlDpnNyhu7Qpj5DayjOU19qP5d1ps3EyPxoQb7ELC9Ep26B5woPwr78gOzyTDFtVsrlcqVR0bifJaVV0qOc0/XoWXrwV02X+aoecZNOPhA/qiOGXFu3gxDV81tHLVLEDOcDYuKu4vTs4uDg4OLm9IzKEbM2fftiuwY7VvKkR53ercRFRx7k/SZtxplO0vXHFoZj1dxthetBgcfuLJ5pX7lpJYE/U6tOkaRJ6ON+5Zwb4dxFQyaRCaPTu036e4zoZKYHR1AZp5T+cJXY+wCmL5TIM5XxXpfJsfanfrMTfLONnFfuO8RXqMRFlj323kv0lzjRyUy3PLiDOFcm0fNUL+IDmP6M5FBBRPifxaavogsUZPajnFcKR0nivYr/88jpzcp0TItxauKTrUu1tevAQTDuLZ7pK7Rec6SYJj5lFj+N0TVXsDicaGgyFZHNIz1+i4fDUkpEHDsgfJx9tfXYxchs35OaXrfaOg9wN6fGfkPXar6prduEaVqV828+nwY4SYl3IwyIv2Z/6gCMCnU0+Ok5RtUk+I6A6QSV56/b4CZcskhFz0M3bWGS2G292K5ce5KNQ7LHT1sC9+PQMu/ko63LieUuqpfyqXuhzlV9iUrg0HuYYOp4gJ9HJTPUgZWyWXu7COutuxLX4nbY3kCbzqkvVetqvAGp7UBkfBfRk7ojzfcdv3K4rB4ZwXdex4OBzKV7eF8P9ah4MedOsL23Y0vshIWodVc+sjqxwYC9uoWvDO4Qv6ZqQuQ+OyU2LR2Bf1Urg/oLHHE52N6vQIm1Ipi9UB4np1rSqhsKTIuN7J1tgPbz1ySik3zfoA6fyo5I2e5WaT95AapbrpoFHNoFb+638Hq44b6HLlTHnLH2Id0XC+pXzmPw+ECFPiqBR0rmyQtwsxvL2s3y9UkZz0fse4NVjzzp8cNBarH2AB3y9431aK9NCLzoyBLk5p2HZQ5PPX3n4h3GPeGmz+sWqeVKDw4dDIO/hi/qeYZ0gfaT1q85rIND5CINUrqsRI7ppEwOVZjnaUJt1UTPY0ZdJFVfypMe2NoimloZ+DuYbrcTiU6udD248JH2EDdWhxyqAyxy0+c7HFQRxm6D20C8tS4Ie7XBZOo0ZPYcDm4xmrN3qqSHyrpvaiRDF6Wi68QylAqmEbPXU0sPcVZYfae1Yvau+lJ7MmWhhx/D2IHSlvUYbfGr47qtKuTC5Z7vGMSiVUMZfJ2ULQ8uRBw6Xd2/t+Dd3OgzVIoC0sO/uab2K5zHaWoN26T7qqpH3piWHVwoQlXi0R3OznR7LclHh0hssoWtLRnk7l9eXi7v+VHHUQGDCUu/2+F43wUZjmXZtB1n4DIiA4yYNtHm+67cMXizyKmo+pK5OwqwW2s0tFQ8vPtuNKKjrqOGSLb4KD3yhrT0OqoUDS4DBjC9IxffjBfwyJEliKDuk433UcBgoBA4Miq2HwUmfTy08cobTme4frtueTa1DE9O5N2CrYaN4bXqoStyo8KKIqcEHVv9SMNaiK81MSq10dmopl0OGTUvwOmYyUVhhXDCeNygCekzGvTmz1Ia0WbSMISBwdFbqWihirei47LvYuiw3f1e6Fn9uxcX3YhhgGx5wNUVUZz7zIYQVJeTmw5yitOioE+KIKZ53RcfejIq05dF0gPfLLnRoRAeHE1XyKOZc/HN5++1NKKTQ3MPqh5G7FpLLb0V3O690FdE8s26x0jXHavpZtySUmPAubxKmNrvw4R4w3qRr+pnNhGjKHlBzH63Dx/qvxGqI33y69TSY+yQ6p0yZxkjZ2F6c+1nqucITSApNJ83wYRJQQBudsO5D201qoCFj+0nUgLpXqL0sLdgYyicjcgVRS+96xjSo3MLrtJpZmurLO49EjS1xHyXuHhhw/jCxIBPcWkz/e4FTz2j9Gi3v3xNN+hSSh1y31EmDIPUwClwdz0kujvkYLpcK3fAbNXgijzilatLRc3qIuE5jvpJITycrkero3GYZUGsSrFj5f3PeJWk9MB0J6+15TtaFg6eEux8Sqbbm5s5PKf0i9axtUVyU/GTO+6h2OOuDocVW2noGOdQqjmPAxsqCY7Q4WN08dY67niW6qeDdDvi66aSopJVvU8yiyfdyGltHe8wEoAw6Eu/OBXT7fZva7/n8KwXzdRxP9m655KCz3I7nX3pKfYdqUI500fZ+PfSfTBPrSESXjpBP+98Er4DshxV5faTxy+ZZrlRvpaeQvkEMxbHVStLiFZf1apy6qxhNqyZNATv6EE+y5u/Pf8M4mAOz8GBJRW6Boxjwd9udiIEgvrlhyoR3cvyOSNWiZcpOtKO8iotD6JOp/ppEPqOyHQ66mJsU5FXaUhUSqMrdYgq5Y4ntko+69IjUVpRPGAhHl00SRXTmW4H2NzcfP7yy9eSojldPiU7jzBPls7Kumz1JPTKv8Ol36b16xBdaDbStZ6ObVgZ3j38dOuAH1SePBB5tAx90o9woR2eUtzxFNtXkVIwq+pNoxyRJ2dtaTILzdrYa68JQifQXnv+28/fv381p7BmgeR4cdcEH82HKQPwBq6cGTIx17/akRbCOdn6KMs4zL48hz/mESSH5OqpoFYqp9ihanfHjxsVojSdq6mlR1hfUnEfE0QpP/bam88/f9Hxxx/fv3/9+rVsihHDU9EcoJJ4TlFrK/xU6yXlAEOmo8sfZ1qMrxdFKOpWYH5DWICCig53aL2atrZSPrSvrkXMplU9KWJ26tZWF1adjtQs+mwpu75t/mJOjuiuzcRxdMDJV1zEL/HRcdeAiJgOv6xH/QnGpV1omiRcfQfLJvIXV3tzJT1SZ+/1MT+PXYwDM2bsu3qATEfXo6kKOA1UtZt/zMhoGlKGu77a8ImpS5ANHGA65mBuHUgp69q+jyDR0WisLa1araRHauNPeQ+zpN2M6ipJZis5Z4jodbSUBR109Lv9czE8p5l01+LwgV7q8H8HbzmtkzKuoyxsavvKQg5Dg+F17XkiPVIbf/Q19NCv4yNycHRwbmvrHEsy0K410FUHX2kF0FmR7KXlX/oSntRIHZG+ZcEBGvUxxPfeloshPDZMwfdw/FOUE3j6Wi4XZPB8qh0SyV15gw/t015XI5IeakDvFNJDT1hUFYTx10WZdEqielcH/R5rR1EcunDxA6E3fKgOBr3WetRTjHbmlr7DmKzvHPqOWKy9Ufl0UrstwhUdzrRxdXNx0f91pA/cE687nVp6PFroOZTfAK9t/2MxTKde73NoyxvxrISgFwnqyMXZrsu5E/bIQfgzZsf+GsGdpywmyCybsbuFSI/0geUXWmZQNks1MzYYdcbWVjeqL0Hl3yAGHQb7fy6EaTPVHY4dkPKxZraGdYf4NsY5l7cgSIyYJvnk16GbED7vxcd+nCW74NRXyXOIn1S8vpQjPY5lExHDB9bWw/9WSkNwRqQXue5tWXjLXBcrariSECf+qzS+Hufy0VF3qDMxeSN5VE0c/awpR4pZ1Ch5s7YGHVLcMah1Cwe4kICYMSWhiTdS5mpNh020UXTA1pBDp6geX1PrzsXAY7OJQX2qBZ45sPwqoV1AhgtVxL5qfFkjb1SNRzIV6UfAxhkfLoLpjKB8vAOmyp3MyXC3TagtwpF6t+euPFpV4QcMYBgIS5DptJnSz/rQszjVZuXsKsasys3Tg2uEH65B8hVV7xA/+ev8POsdixjuPDBVO2cu3HjHJgUD7uw8RMs4i14Ki8uWSwuTw0n3r/p+eRL4Wm/Qmo0rEk0bglklPfJmbYXlGNWlheaSlFR8AdIjU2ZWd5oSO3kLRVbvO44VxkJuO836OLguh7DzTjyPj5yNCDT3k9rxplGRaOQ1/m52G1HbPPjeqLxthy6lUikH/xpyaZjTRvAg2NTIawN8esGuvUGUh4w59r/nZrqcXUqstiTy54f7vR/rQ6fTYVvjlrB//y5l314Hojt3k9735jMgf9LQzfVoIyC5tvt2KuzfP5G7QvyDx/2cNzq+t9GCJ0Oj9a85mTYrC10UyO8eDwbdvAUSujDwPnDSC1hS1fcPDg6255491nP0JNyg5m1YcwZEs/Z5/jOdGft10IPWR0yveyeqOMcEtAfq6vDb+5k2w+bn2Wzz3BeDlgcylddX6I/2PLgGVv9VciijPH/NGr2RR3V5dylrPw7CgWWR1XirtEjwowMNFxUHcfjSHFUPs7GsxQjXHSiYWVv5r/7zcGmTun/si9l/fwfToduoVM6WtZbpYRMiO/+Imbnvhg91B6x0ULynvhToTnN0urRAJCokUSJeX6k/IzMwuAqDGBQhKk5TX9KWMKpUaqPrkyXGe//VhrjuPua//E9E1dU9ByN2HfjpPOlhVmobErujs+urkwXozrnQOopmbtm2c79KvkMW4kF6IMOSfT6cnCSroTwKqJVYDeE6eLqMsHW5Ygu57zuqc2Fg1UOW2e1/ZBdoZ1pr5P8dLxap/oOfBmGdLj3EFUgc0FggBZxjXRq1NBSoWY70mGEGe4Fjh2u1O7DrqF2RXV/KrccWIKi6mCFiO0NaeNjwz5QehfOYBS2XxXNxxTpnmVWPYuXdWRBNaNcctYEpI3/NlB55reACFLeOKv9TSS2QU/Uwi4A4A4Ywy0wVTlWJOru1lbKcY4Fk7GBaqHriSHeq9IjUdLEW/QzodUDbEXWHVl1Ij8UhmrWF0ZChhQv2rUzpkTfLsQDFA1kLmYTCaBMfZsXDUmMZvdi/LER9iTRqoXIqvuWMqslbwLMAxaVl0IHpjOSJGaNq5HItRUCcATZdD1drjQdfdk7Vo2B6enR3QExrzlp6j+zWVu5UgwIEVQ+cBZ1aC5ucRiE9FoWIaTkwHWcwg6i2swNi8ZfhZsDxEddqS7Rjmzdrq/jDcDPhYcdKhZNdX2oUAXEmtNbTkR0Qa8s+9P8hnGYsVZS/ilaB6ZG+VFGpGICwUGxkMZ27gGeBqeEXra0/CReZqyTmzXIsMD0+ZzO97MP7H8JVlvQoFaNqFoe3TJFX1JcWh1Gm9CjqSwsD+SscSUwXra2F4SArIJrpqxcVmBUnWQs05/7ZkgLTI0tOm4XzWCRG8i8eqclZ6rei6LFI+G8bKTgrMvECBf5/8V+AxcF311wU9wAAAABJRU5ErkJggg==");
}

.paymentWrap .paymentBtnGroup .paymentMethod .method.amex {
	background-image: url("https://image.winudf.com/v2/image/Y29tLmNyZWF0aXZlYXBwc2JkLmJrYXNoX3NjcmVlbl8wXzE1MjY5MDkzOTdfMDU1/screen-0.jpg?h=355&fakeurl=1&type=.jpg");
}

.paymentWrap .paymentBtnGroup .paymentMethod .method.vishwa {
	background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARMAAAC3CAMAAAAGjUrGAAAAvVBMVEX///8wPkglsVYiMz4bLjopOEPAw8UsOkUlNUA2RU+xtrrs7e4tPEagpqsjND8Urk5NWWFlb3XU1tn39/jn6ere4OJIVF2T0qWPlpv4/fqrsLRRXGV+hoz7+/zi5OU7SVLIzM7n9+0ptFtud37d8+SYnqNMvnNZZGx1foR/h42vtLi5vcGSmZ42uGTD6dDS79yD0Z1exYGt4b+h2rOb2rBryInt+fLG6tIOJjN+0Jqo3blRwHdyy5C55ciC0ZxGAuz0AAAIDElEQVR4nO2aa2OiOBSGxRBBjVauFa2CouKlVq3VqW3d//+zNheu4sxOd1vF2fN8Ek2AvCTnvDlYKgEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANwSjjNY/Tg+v21f7iNetm/Px6fVwHGufXOXxhmtji+HzWQ4LJfv8pTLw+Fyc3g5rkb7a9/qBXD2o+f73WQphv5LWJPlZLd+/qOF2f/YfizL/6zGiTLl5W77NLj2zX8Dzmi7WZ6T4+zaybdZbl5Gf1SMcX7cTzJjjQa/nLzvPg5rGltD7teHj91mshzm5KEHk/unP0QWZ3U/KSejo2MbLt93axpBB4P9/kyCcZz9fjBYHbd/7d6FOHHf8nK9un1ZnLdNNCY+usnhjaba3+6+Xz29Hd7LiTB3d5vtbcfc0f0wHAydHe8f29W/8h2Os9oe3oexLMP16Mvv9FKMDsO7UI/J4fgfQ6QzelpPQl3uhh+rL7rHyzJal++EILvtVwWB0fZDyHJXPtzgXHkW9z7cHL/WWgyOuyU7NV1BNxdXBm80/W7evsNrDZ553L5bHr/h5N+Lc/y2vOmM1kumysefaG//PYM3ZouXP659Hz/DsLzKvNGezabTh9m45pkXuarzzMzx9iLX+jwebsoyaqoqxqqKZGT7C+MS13We6VxZX+JKn6ciSxkU3OxWLnLl/X25XMz0c6oJBauNy1x7VFD3FmqC6dqhi4fwAyJfSJSCEmry0Kgtao2HFuKqEEm/9n1dE64JqYZxtd6RuCjq9Lp3dV1CTerRsUeYKIT0r3lTV+ZUk9Ij4hOlc8V7ujY5TSzMNZml2tT7fTNjWox+vXRCvf+btsYwrXzvYpHTpN5iiwf74ZE+1lq267p20I7irufTL4JOSoNeI6AtfI91aJzBCptZ82nAT9Z9qETdjX7J6PW+eZSfI6eJ0VViTcy2LSOsKIRC/a4vxhbIhCj4NbW6Fq+YtpE1+rHzinKI1G50NIQwP5eiqHJrIfpaNa9S8y4z2N8kP09sEiUewxapOUKteqzFXOWORkse7pR/IzP/+6DmPKAkPOBMxhnDjMa8b1/3zEqxUn9OE50PSm3Tj3VX4W5fVVVFOLuqFX8tKXFqMvhyIzY7yc818ZkkhJ0MC6VlPtOMimVYxcpyOU3aTT4MvufpYgXh7rTdngWI66D6qSax2fUSGUtjGSeEmiC+TBqIqHSLOWvPfIl3UKpcimLFEkben0jJFxW127F4MDQ8m4uCPHqgE7F4oj4Nnr4VvgCsQEuockmaD7xV3XbburhOXyw21L7UKD9HVpPevErSd6snyUXnDldMBrEMog1AL2CHSpB74B7XBAfhya2kNtPT+Bnsi5QlPo3Y71S9vmn29U6AFTFNrHxLHimULvu4EI85XDx9rhbKuTyLxx0inTlXadGUirutEpoQJFWrhOZdSRwtzrTssBVCXPbQDZdwfcTM6KhnZTRtPhfUs9UYi89Htfa1g/kiovoJIXHaxeo828bUK7VOQywYMfQxjyBEPOYp+yEyeTFGICbT+OT7uu4tOo2ZlJ5pBSNXU1JQy0s3sB41G8vUeWGxqkQkFcHlkX02uaHJTa0pl63pZ6JMvTNtSUgOT1ZwTYiwlxjJ3UY68Hk+tRNKolioSUkTYZU19fiaqp6ESzGTcCu9tTHbEj1ZygYWWxPXrVZdt6WN9fTYem019BgER9FXaFLhFgWxgzZbI82TtFppipJDOogu3NAWU+2LP09olDAtul09zabTqDBJWpofkJQmBrcriC0evnSwl+lohSk9HUPnTUWcDNuB71eLr8nZ3XuDS6KQqWfWjVIFpzQRa0Np9YSBi1JQiMEdS9aT6YRL0gzm/brRM0QQujlNTP6wFdcLW2Y0CWeCLkysms0uM1GX0tLlBF/sHEMVblWTBo8ZxItaZjQRXpYOimdiJWNOOiK+uul3in1Rqopmzq1qwkeN44d9ognviP06Cyw4k3E9vu9VsiZVWN9Yu1vVhAcFHFfwhY+NNeFiEHfOGmWqt6F/bWY96pib3XjqiKV3q5pEpSMRNxNNSmO+ZalKkeEPMTQ+I5onw+WaSMRKd75BTbTM7vdRDCpZEWLrx6PEQ6pXWFxJpxymaiejVEX0vT1NhAq4q/dKhjWN6mde/Lsf14xSkaOGhMezu60YexqV74hUo+HJbIRy5jZDxeAXmliiRohJQP2aGs0KL+kqhi/hIMm58eShG4UYXO1HloWgrq8xP0tuU5NSO9wfcl+PbZcPKtn6iwo/XRCpCOul9kYJrATnRcYeU6kJ4XnndEdQEH6liaGheFjItkRNILX/DeNmurNOzkgiykoNFOuFlfmCW71Z7ppFoPLK3t2Q82/mjLbaxOxlDmrO6nTaKER5TYVFPVOn5Hhy/vUOknlFn+0B2esdrMqBV9JfMVGQVrwCNaXPXup1f7qurXbQsul2mT3o/rRrd6cpx9pJVZlCjNoZ5p74sd7RunYrmHlUCeMxaHW1c/W8ImD0f/2/PsNIHmamSCKKSZlE/I/06skpjEJOkv8GT8+EnCtB/18Rf8oo6juaa2C0xYsu9zJ/qL0Bel5XpOlmMd9GXJ5eTQvrtKiYBuMKmG64+UFaMV9uXgGRhEGSNOINuIJmIElCAxGM7KKa0OtguFjrwCTJUjdBEQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC4Qf4GWQ6anSPmhQAAAAAASUVORK5CYII=");
}

.paymentWrap .paymentBtnGroup .paymentMethod .method:hover {
	border-color: #4cd264;
	outline: none !important;
}

</style>

<body>
    <header id="header"><!--header-->
        <div class="header_top"><!--header_top-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="contactinfo">
                            <ul class="nav nav-pills">
                                <li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i> info@domain.com</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="social-icons pull-right">
                            <ul class="nav navbar-nav">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header_top-->
        
        <div class="header-middle"><!--header-middle-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="logo pull-left">
                            <a href="{{URL::to('/')}}"><img src="{{URL::to('frontend/images/home/logo.png')}}" alt="" /></a>
                        </div>
                        <div class="btn-group pull-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                    Bangladesh
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Bangladesh</a></li>
                                    
                                </ul>
                            </div>
                            
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                    Bangladeshi Taka
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Bangladeshi Taka</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="shop-menu pull-right">
                            <ul class="nav navbar-nav">
                                <li><a href="#"><i class="fa fa-user"></i> Account</a></li>
                                <li><a href="#"><i class="fa fa-star"></i> Wishlist</a></li>
                                
                                <?php  $customer_id=Session::get('customer_id');
                                        $shipping_id=Session::get('shipping_id');
                                ?>

                                <?php if($customer_id != NULL && $shipping_id == NULL){ ?>
                                         <li><a href="{{URL::to('/checkout')}}">Checkout</a></li>   
                                <?php } if($customer_id != NULL && $shipping_id != NULL) { ?>
                                        <li><a href="{{URL::to('/payment')}}">Checkout</a></li>
                                
                                <?php } else { ?>
                                        <li><a href="{{URL::to('/login-check')}}">Checkout</a></li>

                                <?php } ?>
                                
                                <li><a href="{{URL::to('/show-cart')}}"><i class="fa fa-shopping-cart"></i> Cart</a></li>

                                <?php  $customer_id=Session::get('customer_id');  ?>

                                <?php 

                                    if($customer_id != NULL){ ?>
                                        <li><a href="{{URL::to('/customer_logout')}}"><i class="fa fa-lock"></i> Logout</a></li>           
                                    <?php } else { ?>
                                        <li><a href="{{URL::to('/login-check')}}"><i class="fa fa-lock"></i> Login</a></li> 
                                        
                                    <?php } ?>    

                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header-middle-->
    
        <div class="header-bottom"><!--header-bottom-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="mainmenu pull-left">
                            <ul class="nav navbar-nav collapse navbar-collapse">
                                <li><a href="{{URL::to('/')}}" class="active">Home</a></li>
                                <li class="dropdown"><a href="#">Shop<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="shop.html">Products</a></li>
                                        <li><a href="product-details.html">Product Details</a></li> 
                                        <?php  $customer_id=Session::get('customer_id');  ?>

                                <?php 

                                    if($customer_id != NULL){ ?>
                                         <li><a href="{{URL::to('/checkout')}}"> Checkout</a></li>   
                                <?php } else { ?>
                                    <li><a href="{{URL::to('/login-check')}}"> Checkout</a></li>
                                
                                <?php } ?>
                                        <li><a href="{{URL::to('/show-cart')}}">Cart</a></li> 
                                    </ul>
                                </li> 
                                <li class="dropdown"><a href="#">Blog<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="blog.html">Blog List</a></li>
                                        <li><a href="blog-single.html">Blog Single</a></li>
                                    </ul>
                                </li> 
                                <li><a href="404.html">404</a></li>
                                <li><a href="contact-us.html">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="search_box pull-right">
                            <input type="text" placeholder="Search"/>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header-bottom-->
    </header><!--/header-->
    


    @yield('slider')

    <?php 
        $all_published_slider=DB::table('tbl_slider')
        ->where('publication_status',1)
        ->get(); 

    ?>
    
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <h2>Category</h2>
                        <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                            <div class="panel panel-default">
                            <?php
                                $all_published_category = DB::table('tbl_category')
                                                            ->where('publication_status', 1)
                                                            ->get();
                                foreach($all_published_category as $v_category){ ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                <h4 class="panel-title"><a href="{{ URL::to('/product_by_category/'.$v_category->category_id) }}">{{$v_category->category_name}}</a></h4>
                                </div>
                            </div>
                        <?php } ?>
                            </div>
                        </div><!--/category-products-->
                    
                        <div class="brands_products"><!--brands_products-->
                            <h2>Brands</h2>
                            <div class="brands-name">
                                <ul class="nav nav-pills nav-stacked">
                                        <?php
                                        $all_published_manufacture = DB::table('manufacture')
                                                                    ->where('publication_status', 1)
                                                                    ->get();
                                        foreach($all_published_manufacture as $v_manufacture){ ?>
                                    <li><a href="{{ URL::to('/product_by_manufacture/'.$v_manufacture->manufacture_id) }}"> <span class="pull-right"></span>{{$v_manufacture->manufacture_name}}</a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div><!--/brands_products-->
                        
                        <div class="price-range"><!--price-range-->
                            <h2>Price Range</h2>
                            <div class="well text-center">
                                 <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
                                 <b class="pull-left">$ 0</b> <b class="pull-right">$ 600</b>
                            </div>
                        </div><!--/price-range-->
                        
                        <div class="shipping text-center"><!--shipping-->
                            <img src="images/home/shipping.jpg" alt="" />
                        </div><!--/shipping-->
                    
                    </div>
                </div>
                
                <div class="col-sm-9 padding-right">
                    
                    @yield('content')
                    
                </div>
            </div>
        </div>
    </section>
    
    <footer id="footer"><!--Footer-->
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="companyinfo">
                            <h2><span>e</span>-shopper</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor</p>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="col-sm-3">
                            <div class="video-gallery text-center">
                                <a href="#">
                                    <div class="iframe-img">
                                        <img src="{{URL::to('frontend/images/home/iframe1.png')}}" alt="" />
                                    </div>
                                    <div class="overlay-icon">
                                        <i class="fa fa-play-circle-o"></i>
                                    </div>
                                </a>
                                <p>Circle of Hands</p>
                                <h2>24 DEC 2014</h2>
                            </div>
                        </div>
                        
                        <div class="col-sm-3">
                            <div class="video-gallery text-center">
                                <a href="#">
                                    <div class="iframe-img">
                                        <img src="{{URL::to('frontend/images/home/iframe2.png')}}" alt="" />
                                    </div>
                                    <div class="overlay-icon">
                                        <i class="fa fa-play-circle-o"></i>
                                    </div>
                                </a>
                                <p>Circle of Hands</p>
                                <h2>24 DEC 2014</h2>
                            </div>
                        </div>
                        
                        <div class="col-sm-3">
                            <div class="video-gallery text-center">
                                <a href="#">
                                    <div class="iframe-img">
                                        <img src="{{URL::to('frontend/images/home/iframe3.png')}}" alt="" />
                                    </div>
                                    <div class="overlay-icon">
                                        <i class="fa fa-play-circle-o"></i>
                                    </div>
                                </a>
                                <p>Circle of Hands</p>
                                <h2>24 DEC 2014</h2>
                            </div>
                        </div>
                        
                        <div class="col-sm-3">
                            <div class="video-gallery text-center">
                                <a href="#">
                                    <div class="iframe-img">
                                        <img src="{{URL::to('frontend/images/home/iframe4.png')}}" alt="" />
                                    </div>
                                    <div class="overlay-icon">
                                        <i class="fa fa-play-circle-o"></i>
                                    </div>
                                </a>
                                <p>Circle of Hands</p>
                                <h2>24 DEC 2014</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="address">
                            <img src="images/home/map.png" alt="" />
                            <p>505 S Atlantic Ave Virginia Beach, VA(Virginia)</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="footer-widget">
            <div class="container">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="single-widget">
                            <h2>Service</h2>
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="#">Online Help</a></li>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">Order Status</a></li>
                                <li><a href="#">Change Location</a></li>
                                <li><a href="#">FAQ’s</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="single-widget">
                            <h2>Quock Shop</h2>
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="#">T-Shirt</a></li>
                                <li><a href="#">Mens</a></li>
                                <li><a href="#">Womens</a></li>
                                <li><a href="#">Gift Cards</a></li>
                                <li><a href="#">Shoes</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="single-widget">
                            <h2>Policies</h2>
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="#">Terms of Use</a></li>
                                <li><a href="#">Privecy Policy</a></li>
                                <li><a href="#">Refund Policy</a></li>
                                <li><a href="#">Billing System</a></li>
                                <li><a href="#">Ticket System</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="single-widget">
                            <h2>About Shopper</h2>
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="#">Company Information</a></li>
                                <li><a href="#">Careers</a></li>
                                <li><a href="#">Store Location</a></li>
                                <li><a href="#">Affillate Program</a></li>
                                <li><a href="#">Copyright</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3 col-sm-offset-1">
                        <div class="single-widget">
                            <h2>About Shopper</h2>
                            <form action="#" class="searchform">
                                <input type="text" placeholder="Your email address" />
                                <button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
                                <p>Get the most recent updates from <br />our site and be updated your self...</p>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <p class="pull-left">Copyright © 2013 E-SHOPPER Inc. All rights reserved.</p>
                    <p class="pull-right">Designed by <span><a target="_blank" href="http://www.themeum.com">Themeum</a></span></p>
                </div>
            </div>
        </div>
        
    </footer><!--/Footer-->
    

  
    <script src="{{asset('frontend/js/jquery.js')}}"></script>
    <script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('frontend/js/jquery.scrollUp.min.js')}}"></script>
    <script src="{{asset('frontend/js/price-range.js')}}"></script>
    <script src="{{asset('frontend/js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('frontend/js/main.js')}}"></script>
</body>
</html>